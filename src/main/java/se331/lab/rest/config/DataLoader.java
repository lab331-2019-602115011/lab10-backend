package se331.lab.rest.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Lecturer;
import se331.lab.rest.entity.Student;
import se331.lab.rest.repository.CourseRepository;
import se331.lab.rest.repository.LecturerRepository;
import se331.lab.rest.repository.StudentRepository;


@Component
public class DataLoader implements ApplicationRunner {
    @Autowired
    StudentRepository studentRepository;
    @Autowired
    LecturerRepository lecturerRepository;
    @Autowired
    CourseRepository courseRepository;

    @Override
    @Transactional
    public void run(ApplicationArguments args) throws Exception {
        Student student1 = Student.builder()
                .id(1l)
                .studentId("SE-001")
                .name("Prayuth")
                .surname("The minister")
                .gpa(3.59)
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/tu.jpg?alt=media&token=f16b5c1c-fbea-4d98-9fa3-732b69a1bae8")
                .penAmount(15)
                .description("The great man ever!!!!")
                .build();
        Student student2 = Student.builder()
                .id(2l)
                .studentId("SE-002")
                .name("Cherprang ")
                .surname("BNK48")
                .gpa(4.01)
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/cherprang.png?alt=media&token=2e6a41f3-3bf0-4e42-ac6f-8b7516e24d92")
                .penAmount(2)
                .description("Code for Thailand")
                .build();
        Student student3 = Student.builder()
                .id(3l)
                .studentId("SE-003")
                .name("Nobi")
                .surname("Nobita")
                .gpa(1.77)
                .image("https://firebasestorage.googleapis.com/v0/b/test-331-lab05.appspot.com/o/nobita.jpg?alt=media&token=16e30fb0-9904-470f-b868-c35601df8326")
                .penAmount(0)
                .description("Welcome to Olympic")
                .build();
        Student student4 = Student.builder()
                .id(4l)
                .studentId("SE-010")
                .name("Koy")
                .surname("Ruchawin")
                .gpa(2.01)
                .image("https://praew.com/app/uploads/2018/03/22426752_390853657997701_6631203095822991360_n-e1522216763104.jpg")
                .penAmount(1)
                .description("Good runner")
                .build();
        Student student5 = Student.builder()
                .id(5l)
                .studentId("SE-011")
                .name("Nui")
                .surname("Amphon")
                .gpa(3.52)
                .image("https://img.thaipr.net/thaipr/300x250c/prml/7/FEDE863AD2BC7B5FD784BB61D8BF724B.jpg")
                .penAmount(2)
                .description("Good singer")
                .build();
        Student student6 = Student.builder()
                .id(6l)
                .studentId("SE-012")
                .name("O")
                .surname("Kung")
                .gpa(1.49)
                .image("https://pbs.twimg.com/media/Dm6LuwlXgAAIvCq.jpg")
                .penAmount(3)
                .description("Who ?")
                .build();

        studentRepository.save(student1);
        studentRepository.save(student2);
        studentRepository.save(student3);
        studentRepository.save(student4);
        studentRepository.save(student5);
        studentRepository.save(student6);

        Lecturer lecturer1 = Lecturer.builder()
                .name("Chartchai")
                .surname("Doungsa-ard")
                .build();
        Lecturer lecturer2 = Lecturer.builder()
                .name("Jayakrit")
                .surname("Hirisajja")
                .build();
        Lecturer lecturer3 = Lecturer.builder()
                .name("Kong")
                .surname("Kasit")
                .build();

        this.lecturerRepository.save(lecturer1);
        this.lecturerRepository.save(lecturer2);
        this.lecturerRepository.save(lecturer3);

        Course course1 = Course.builder()
                .courseId("953331")
                .courseName("Component Based Software Dev")
                .content("Noting just for fun")
                .build();
        Course course2 = Course.builder()
                .courseId("953XXX")
                .courseName("X project")
                .content("Do not know what to study")
                .build();
        Course course3 = Course.builder()
                .courseId("001001")
                .courseName("English for everyday life")
                .content("No content")
                .build();
        Course course4 = Course.builder()
                .courseId("953100")
                .courseName("Learning Through Activity")
                .content("Learn everything")
                .build();

        this.courseRepository.save(course1);
        this.courseRepository.save(course2);
        this.courseRepository.save(course3);
        this.courseRepository.save(course4);

        //Lecturer get students and, add students to advisor
        lecturer1.getAdvisees().add(student1);
        student1.setAdvisor(lecturer1);

        lecturer1.getAdvisees().add(student2);
        student2.setAdvisor(lecturer1);

        lecturer2.getAdvisees().add(student3);
        student3.setAdvisor(lecturer2);

        lecturer3.getAdvisees().add(student1);
        lecturer3.getAdvisees().add(student2);
        lecturer3.getAdvisees().add(student3);
        lecturer3.getAdvisees().add(student4);
        lecturer3.getAdvisees().add(student5);
        lecturer3.getAdvisees().add(student6);
        student1.setAdvisor(lecturer3);
        student2.setAdvisor(lecturer3);
        student3.setAdvisor(lecturer3);
        student4.setAdvisor(lecturer3);
        student5.setAdvisor(lecturer3);
        student6.setAdvisor(lecturer3);

        //Students enrolled courses and courses get students
        student1.getEnrolledCourses().add(course1);
        course1.getStudents().add(student1);
        student1.getEnrolledCourses().add(course2);
        course2.getStudents().add(student1);
        student2.getEnrolledCourses().add(course1);
        course1.getStudents().add(student2);
        student3.getEnrolledCourses().add(course2);
        course2.getStudents().add(student3);
        student4.getEnrolledCourses().add(course2);
        student4.getEnrolledCourses().add(course4);
        course2.getStudents().add(student4);
        course4.getStudents().add(student4);
        student5.getEnrolledCourses().add(course2);
        student5.getEnrolledCourses().add(course4);
        course2.getStudents().add(student5);
        course4.getStudents().add(student5);
        student6.getEnrolledCourses().add(course2);
        student6.getEnrolledCourses().add(course4);
        course2.getStudents().add(student6);
        course4.getStudents().add(student6);

        //set courses to lecturer
        course1.setLecturer(lecturer2);
        lecturer2.getCourses().add(course1);
        course2.setLecturer(lecturer1);
        lecturer1.getCourses().add(course2);
        course3.setLecturer(lecturer1);
        lecturer1.getCourses().add(course3);
        course4.setLecturer(lecturer3);
        lecturer3.getCourses().add(course4);

        //update again
        studentRepository.save(student1);
        studentRepository.save(student2);
        studentRepository.save(student3);
        studentRepository.save(student4);
        studentRepository.save(student5);
        studentRepository.save(student6);
    }


}

