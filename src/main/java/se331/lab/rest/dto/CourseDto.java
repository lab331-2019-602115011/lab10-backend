package se331.lab.rest.dto;


import lombok.*;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class CourseDto {
    String courseId;
    LecturerDto lecturer;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    List<StudentDto> students;

}
