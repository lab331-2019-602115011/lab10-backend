package se331.lab.rest.dto;

import lombok.*;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class LecturerDto {
    Long id;
    String name;
    String surname;

    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    List<StudentDto> advisees;
}
